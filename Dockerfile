FROM ubuntu:focal

RUN DEBIAN_FRONTEND=nointeractive apt-get update && apt-get install -y --no-install-recommends \
    cython python3 python3-pip python3-dev python3-setuptools build-essential git \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean

# install the notebook package
RUN pip3 install --upgrade pip
RUN pip3 install pyzmq --install-option="--zmq=bundled"
RUN pip3 install notebook
